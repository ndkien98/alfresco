package com.vsii.alfresco.service.impl;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

@Service
public class FolderServiceImpl {

    private final Session session;

    public FolderServiceImpl(Session session) {
        this.session = session;
    }


    Folder createFolder(String folderName){
        Folder rootFolder = session.getRootFolder();

        // create folder
        Map<String,Object> properties = new HashMap<>();
        properties.put(PropertyIds.OBJECT_TYPE_ID,"cmis:folder");
        properties.put(PropertyIds.NAME,folderName);

        return rootFolder.createFolder(properties);
    }

//    Document createDocument(String fileName,String parentFolderName){
//
//        Map<String,Object> propertiesFileOfFolderTest =  new HashMap<>();
//        propertiesFileOfFolderTest.put(PropertyIds.OBJECT_TYPE_ID,"cmis:document");
//        propertiesFileOfFolderTest.put(PropertyIds.NAME,"FolderTest");
//
//        byte[] contentOfDocument = "hello ae".getBytes();
//        InputStream stream = new ByteArrayInputStream(contentOfDocument);
//        ContentStream contentStream = new ContentStreamImpl("FolderTest", BigInteger.valueOf(contentOfDocument.length),"text/plain",stream);
//
//        Folder rootFolder = session.getRootFolder();
//        rootFolder.getFolderTree()
//
//        Document document = folderTest.createDocument(propertiesFileOfFolderTest,contentStream, VersioningState.MAJOR);
//    }

    public static void main(String[] args) {


        SessionFactory factory = SessionFactoryImpl.newInstance();
        Map<String,String> parameter = new HashMap<String,String>();
        parameter.put(SessionParameter.USER,"admin");
        parameter.put(SessionParameter.PASSWORD,"admin");
        parameter.put(SessionParameter.ATOMPUB_URL,"http://192.168.0.112:8080/alfresco/api/-default-/public/cmis/versions/1.0/atom");
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

        Session session = factory.getRepositories(parameter).get(0).createSession();
        Folder root = session.getRootFolder();

        // create folder
        Map<String,Object> properties = new HashMap<>();
        properties.put(PropertyIds.OBJECT_TYPE_ID,"cmis:folder");
        properties.put(PropertyIds.NAME,"FolderTest");

        Folder folderTest = root.createFolder(properties);
        Map<String,Object> propertiesFileOfFolderTest =  new HashMap<>();
        propertiesFileOfFolderTest.put(PropertyIds.OBJECT_TYPE_ID,"cmis:document");
        propertiesFileOfFolderTest.put(PropertyIds.NAME,"FolderTest");

        byte[] contentOfDocument = "hello ae".getBytes();
        InputStream stream = new ByteArrayInputStream(contentOfDocument);
        ContentStream contentStream = new ContentStreamImpl("FolderTest", BigInteger.valueOf(contentOfDocument.length),"text/plain",stream);

        Document document = folderTest.createDocument(propertiesFileOfFolderTest,contentStream, VersioningState.MAJOR);

        System.out.println("finish!");


    }
}
