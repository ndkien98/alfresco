package com.vsii.alfresco.config;


import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class AppConfig {

    @Bean
    Session session (){
        SessionFactory factory = SessionFactoryImpl.newInstance();
        Map<String,String> parameter = new HashMap<>();
        parameter.put(SessionParameter.USER,"admin");
        parameter.put(SessionParameter.PASSWORD,"admin");
        parameter.put(SessionParameter.ATOMPUB_URL,"http://192.168.0.112:8080/alfresco/api/-default-/public/cmis/versions/1.0/atom");
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

        return factory.getRepositories(parameter).get(0).createSession();
    }


}
